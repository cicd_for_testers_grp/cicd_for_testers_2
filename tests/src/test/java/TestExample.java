import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static io.restassured.RestAssured.get;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestExample {
    Properties properties;

    {
        properties = new Properties();
        try {
            properties.load(new FileReader("./src/test/resources/local.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getEmployee() throws IOException {
        return Arrays.asList(Files.readString(Paths.get("./src/test/resources/getEmployeeTestData.txt")).split("test\\d+:"));
    }

    @ParameterizedTest
    @MethodSource("getEmployee")
    public void getEmployeeOk(String testData) {
        //Arrange
        JsonObject testDataJson = new Gson().fromJson(testData, JsonObject.class);
        String endpoint = testDataJson.get("endpoint").getAsString();
        String referenceResp = testDataJson.get("expectedResponse").toString();

        //Act
        String response = get(properties.getProperty("base_url") + endpoint).then()
                .assertThat().statusCode(200).log().body()
                .and().extract().asString();
        //Assert
        assertEquals(response, referenceResp);
    }

}
